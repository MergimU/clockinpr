<?php
include ('../includes/functions.php');
/**
 * Created by PhpStorm.
 * User: Mergim
 * Date: 2/1/2017
 * Time: 3:11 PM
 */
session_start();
session_destroy();
redirect_to('index.php');
exit;