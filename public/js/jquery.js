$(document).ready(function () {
    var welcome = $('#welcome').text();
    var email = welcome.substring(8);

//getting the state when user  logged in to system
    $.ajax({
        type: "GET",
        data: {email: email},
        url: 'getstate.php',
        success: function (response) {
            debugger;
            var result = JSON.parse(response)[0];
            if(result.state == 0){
                $('#clockout').addClass('disabled');
                $('#pause').addClass('disabled');
                $('#resume').addClass('disabled');
                document.getElementById('current-state').innerHTML = "You are Clocked Out. Are you ready to Clock In?"
            }
            else if (result.state == 1){
                $('#clockin').addClass('disabled');
                $('#clockout').removeClass('disabled');
                $('#pause').removeClass('disabled');
                $('#resume').addClass('disabled');
                document.getElementById('current-state').innerHTML = "You are Clocked In!"

            }
            else if(result.state == 2){
                $('#clockin').addClass('disabled');
                $('#clockout').removeClass('disabled');
                $('#pause').addClass('disabled');
                $('#resume').removeClass('disabled');
                document.getElementById('current-state').innerHTML = "You are currently in Pause!"
            }
            else if (result.state == 3){
                $('#clockin').addClass('disabled');
                $('#clockout').removeClass('disabled');
                $('#pause').removeClass('disabled');
                $('#resume').addClass('disabled');
                document.getElementById('current-state').innerHTML = "You just resume the work!"
            }
        }
    });
 //making disable the other buttons except clockin button
    $('#clockout').addClass('disabled');
    $('#pause').addClass('disabled');
    $('#resume').addClass('disabled');

//operations when clockin button is clicked
    $('#clockin').click(function () {
        var current_date = $('#datelabel').text();
        var current_time = $('#timelabel').text();

        var obj = {
            date: current_date,
            email: email,
            time: current_time
        };
        console.log(obj);
        $.ajax({
            type: "POST",
            data: obj,
            url: 'clockin.php',
            success: function () {
                alert('You have clocked in!');
                $('#clockin').addClass('disabled');
                $('#clockout').removeClass('disabled');
                $('#pause').removeClass('disabled');
                $('#resume').addClass('disabled');
                document.getElementById('current-state').innerHTML = "You are Clocked In!"
            }

        });
    });
    $('#pause').click(function () {
        ajaxOperation("pause");
        document.getElementById('current-state').innerHTML = "You are currently in Pause!"
    });
    $('#resume').click(function () {
       ajaxOperation("resume");
        document.getElementById('current-state').innerHTML = "You just resume the work!"
    });
    $('#clockout').click(function () {
        ajaxOperation('clockout');
        document.getElementById('current-state').innerHTML = "You are Clocked Out. Are you ready to Clock In?"
    })
function ajaxOperation(operation){
        $.ajax({
            type:"GET",
            data: {email:email},
            url:'getajax.php',
            success: function (response) {
                debugger;
                //parse the row fetched last from clocking
                var result = JSON.parse(response)[0];
                var $id = result.id;
                var $state = result.state;
                var $hours = result.hours;
                var $start = result.start_at;
                var $resume = result.resume_at;
                var obj = {
                    id:$id,
                    state:$state,
                    hours: $hours,
                    start: $start,
                    resume: $resume,
                    operation:operation
                };
                console.log(obj);
                $.ajax({
                    type: "POST",
                    data: obj,
                    url: 'pause.php',
                    success: function (response) {
                        if (operation == 'pause'){
                            $('#clockin').addClass('disabled');
                            $('#clockout').removeClass('disabled');
                            $('#pause').addClass('disabled');
                            $('#resume').removeClass('disabled');
                        }
                        else if (operation =='resume'){
                            $('#clockin').addClass('disabled');
                            $('#clockout').removeClass('disabled');
                            $('#pause').removeClass('disabled');
                            $('#resume').addClass('disabled');
                        }
                        else if (operation =='clockout'){
                            $('#clockin').removeClass('disabled');
                            $('#clockout').addClass('disabled');
                            $('#pause').addClass('disabled');
                            $('#resume').addClass('disabled');
                        }
                        alert();
                    }
                });
            }
        });
}

});

