<?php

include ('../includes/database.php');
include('../includes/layouts/admin-design.php');

$name = (isset($_POST['name']) ? trim(($_POST['name'])) : null);
$surname = (isset($_POST['surname']) ? trim(($_POST['surname'])) : null);
$email = (isset($_POST['email']) ? trim(($_POST['email'])) : null);
$password = (isset($_POST['password']) ? trim(($_POST['password'])) : null);
$phone = (isset($_POST['phone']) ? trim(($_POST['phone'])) : null);
$city = (isset($_POST['city']) ? trim(($_POST['city'])) : null);
$position = (isset($_POST['position']) ? trim(($_POST['position'])) : null);
$password_encrypted = md5($password);
$insert = "insert into user(name,surname, email,password,phone,position_id,city_id) values('$name','$surname','$email',
            '$password_encrypted','$phone', '$position','$city')";
$select = "select * from user where email = '$email'";
$count = "select count(*) from user where email = '$email'";


if (!empty($name) && !empty($surname) && !empty($email) && !empty($password) && !empty($phone) && !empty($city) && !empty($position)) {
    if (strlen($password) > 5 && strlen($password) < 15) {
        $getUser = $conn->prepare($count);
        $getUser->execute();
        $count1 = $getUser->fetchColumn();
        if ($count1 == 0) {
            $insertUser = $conn->prepare($insert);
            $insertUser->execute();
            $_SESSION["success-message"] = "User created successfully!";
        } else {
            $_SESSION["error-message"] = "This user already exist!";
        }
    } else {
        $error_message = "Password must be longer than 5 and shorter than 15 chars!";
        $_SESSION["error-message"] = "Password must be longer than 5 and shorter than 15 chars!";
    }
} else {
    $_SESSION["error-message"] = "All fields are mandatory!";
    // redirect_to('new-user.php');
}
?>




<div class="newuser">
    <div class="newuser-form">
        <form action="new-user.php" class="adduser" method="POST">
            <fieldset>

                <!-- Form Name -->
                <legend align="center">Add New User</legend>
                <!-- Text input-->
                <?php echo error_message();?>
                <?php  echo success_message();?>
                <br>
                <div class="controls">
                    <label class="col-md-4 control-label" for="name">Name:</label>
                    <div class="col-md-12">
                        <input id="name" name="name" type="text" placeholder="name" class="form-control input-md">

                    </div>
                    <label class="col-md-4 control-label" for="surname">Surname:</label>
                    <div class="col-md-12">
                        <input id="surname" name="surname" type="text" placeholder="surname" class="form-control input-md">

                    </div>
                    <label class="col-md-4 control-label" for="email">Email:</label>
                    <div class="col-md-12">
                        <input id="email" name="email" type="text" placeholder="email" class="form-control input-md">

                    </div>
                    <label class="col-md-4 control-label" for="password">Password:</label>
                    <div class="col-md-12">
                        <input id="password" name="password" type="text" placeholder="password" class="form-control input-md">

                    </div>
                    <label class="col-md-4 control-label" for="phone">Phone:</label>
                    <div class="col-md-12">
                        <input id="phone" name="phone"  type="text" placeholder="phone" class="form-control input-md">

                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="city">Select City</label>
                        <div class="col-md-12">
                            <select id="city" name="city" class="form-control">
                                <option value="1">Prishtina</option>
                                <option value="2">Prizreni</option>
                                <option value="3">Gjilani</option>
                                <option value="4">Pejë</option>
                                <option value="5">Mitrovicë</option>
                                <option value="6">Ferizaj</option>
                                <option value="7">Gjakovë</option>
                                <option value="8">Vushtrri</option>
                                <option value="9">Podujevë</option>
                                <option value="10">Rahovec</option>
                                <option value="11">Fushë Kosovë</option>
                                <option value="12">Suharekë</option>
                                <option value="13">Kaçanik</option>
                                <option value="14">Skenderaj</option>
                                <option value="15">Kamenicë</option>
                                <option value="16">Shtime</option>
                                <option value="17">Lipjan</option>
                                <option value="18">Obiliq</option>
                                <option value="19">Drenas</option>
                                <option value="20">Junik</option>
                                <option value="21">Klinë</option>
                                <option value="22">Mamushë</option>
                                <option value="23">Istog</option>
                                <option value="24">Viti</option>
                                <option value="25">Deçan</option>
                                <option value="26">Leposaviq</option>
                                <option value="27">Malishevë</option>
                                <option value="28">Graçanicë</option>
                                <option value="29">Han i Elezit</option>
                                <option value="30">Zubin Potok</option>
                                <option value="31">Zveçan</option>
                                <option value="32">Shtërpcë</option>
                                <option value="33">Dragash</option>
                                <option value="34">Kllokot</option>
                                <option value="35">Ranillug</option>
                                <option value="36">Partesh</option>
                                <option value="37">Novobërdë</option>
                            </select>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="position">Select Position of Employee:</label>
                        <div class="col-md-12">
                            <select id="position" name="position" class="form-control">
                                <option value="1">Admin</option>
                                <option value="2">Employee</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                            <input type="submit" id="btn" name="btn" style="width:200px; margin-left:135px; " class="btn btn-primary" value="Save User">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
</div>
<script src="" type="text/javascript">

    $(document).ready(function () {
        alert(';;;');
       /* $("li").on("click", function (e) {
            e.preventDefault();
            $(this).siblings("li").css("fontWeight", "normal");
            $(this).css("fontWeight", "bold");
            window.alert("The link was clicked");
        });*/
    });
</script>
</body>
</html>
