<?php
include('../includes/layouts/admin-design.php');
?>
<div class="btn-inputs">
    <div class="para">
        <p style="color: white">h</p>
    </div>
    <div class="date-head">
        <h4>Current Time: <span id="timelabel"><?php echo date("H:i:s A");?></span></h4>
        <h4>Current Date: <span id="datelabel"><?php echo date("Y/m/d");?></span></h4>
    </div>

    <div class="two">
        <a href="#" class="btn btn-primary btn-primary" id="clockin"><span class="glyphicon glyphicon-circle-arrow-right"></span> Clock In</a>
        <a href="#" class="btn btn-primary btn-primary" id="clockout"><span class="glyphicon glyphicon-circle-arrow-left"></span> Clock Out</a>
    </div>

    <div class="four">
        <a href="#" class="btn btn-primary btn-warning" id="pause"><span class="glyphicon glyphicon-hand-left"></span>  Pause</a>
        <a href="#" class="btn btn-primary btn-warning" id="resume"><span class="glyphicon glyphicon-hand-right"></span>  Resume</a>
    </div>
</div>
<div>
    <p id="current-state">

    </p>
</div>

<script src="../public/js/time.js" type="text/javascript">

</script>
<script src="../public/js/jquery.js">

</script>
</body>
</html>

