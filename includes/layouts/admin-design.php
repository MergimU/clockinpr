<?php
include('../includes/layouts/headerUsers.php');
?>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<div class="nav-side-menu">
    <d class="brand">Dashboard</d>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">

            <li>
                <a href="new-user.php" id="newuser">
                    <i class="glyphicon glyphicon-plus"></i> Add New User <span class="glyphicon glyphicon-menu-right" id="g1position"></span>
                </a>
            </li>

            <li>
                <a href="report.php" id="report" >
                    <i class="glyphicon glyphicon-list "></i> Generate Reports <span class="glyphicon glyphicon-menu-right" id="g2position"></span>
                </a>

            </li>
        </ul>
    </div>
</div>