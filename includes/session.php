<?php
session_start();
function session_message(){
    if(isset($_SESSION["message"])){
        $output = "<div class = \"message\">";
        $output .= htmlentities($_SESSION["message"]);
        $output .= "</div>";
        $_SESSION['message'] = null;
        return $output;
    }
}

function success_message(){
    if(isset($_SESSION["success-message"])){
        $output = "<div id = \"success-message\">";
        $output .= htmlentities($_SESSION["success-message"]);
        $output .= "</div>";
        $_SESSION['success-message'] = null;
        return $output;
    }
}

function error_message(){
    if(isset($_SESSION["error-message"])){
        $output = "<div id = \"error-message\">";
        $output .= htmlentities($_SESSION["error-message"]);
        $output .= "</div>";
        $_SESSION['error-message'] = null;
        return $output;
    }
}